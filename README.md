**To Run this HTML file as website**
Different ways of running this file is as follows:


**Through local storage**
1. Download the folder.
2. Just double click on html file.
3. Make sure it opens in browser.

OUTPUT: Website will be visible with URL address denoting local path

**Through live server**
1. Download the folder.
2. Drag and drop folder in visual studio.
3. Install live server.
4. Right click on the file and run as Live Server.

OUTPUT: This should open file with local ip address in URL.

**Through github repository**
1. Clone or Fork the repository to your account.
2. Change the repository setup to master or main as per preference.
3. Publish the website.
4. Use the link appearing on the refresh.

OUTPUT: Website should be hosted from the repository.

**LICENCE**
Though this website is opensourced, I would like to maintain some terms and condition for my website which can be viewed in detail in LICENCE.txt file.

Have Fun.


